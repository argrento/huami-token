"""
Entry point when using huami_token as a module 
"""

from .huami_token import main

if __name__ == "__main__":
    main()
